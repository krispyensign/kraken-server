import { Logger } from 'winston'
import { Publisher, Pull } from 'zeromq'
import { ConnectionManager } from 'socket-comms-libs'
import { sleep } from 'kraken-helpers'
import { Encoder, Decoder } from '@msgpack/msgpack'
import WebSocket from 'ws'

export interface ExchangeAdapter {
  getReqId(parsedEvent: unknown): string
  isEvent(parsedEvent: unknown): boolean
}

export class OrdersService {
  private readonly logger: Logger
  private readonly authWS: WebSocket
  private readonly encoder: Encoder<unknown>
  private readonly decoder: Decoder<unknown>
  private readonly workResponseZ: Publisher
  private readonly exchangeAdapter: ExchangeAdapter
  private readonly workRequestZ: Pull
  private isRunning = true

  constructor(
    logger: Logger,
    connectionManager: ConnectionManager,
    exchangeAdapter: ExchangeAdapter
  ) {
    this.logger = logger
    this.authWS = connectionManager.getAuthWS()
    this.encoder = connectionManager.getEncoder()
    this.decoder = connectionManager.getDecoder()
    this.workResponseZ = connectionManager.getWorkResponder()
    this.workRequestZ = connectionManager.getWorkConsumer()
    this.exchangeAdapter = exchangeAdapter
  }

  private async shutdown(): Promise<void> {
    this.logger.info('Got shutdown')
    this.authWS.close()
    this.workRequestZ.close()
    this.workResponseZ.close()
    this.isRunning = false
    this.logger.info('Shutdown complete')
  }

  public async startClient(): Promise<Error | void> {
    // setup the message event
    this.authWS.on('message', async (eventData: string) => {
      // log the data
      this.logger.info(eventData)

      // parse it
      let parsedEvent = JSON.parse(eventData)

      // check if its a status event.  if so then get the reqid for the topic
      let reqid = '0'
      if (this.exchangeAdapter.isEvent(parsedEvent)) {
        this.logger.info(`Recv: ${eventData}`)
        reqid = this.exchangeAdapter.getReqId(parsedEvent)
      }

      // send the work response with the reqid as the topic
      await this.workResponseZ.send([reqid, this.encoder.encode(parsedEvent)])
    })

    // wait for it to connect completely
    while (this.authWS.readyState === 0) await sleep(100)
    this.logger.info('Socket ready state: ' + this.authWS.readyState)
    this.logger.info('Waiting for shutdown')

    process.once('SIGINT', () => this.shutdown())
    process.once('SIGUSR2', () => this.shutdown())

    // start polling the zmq socket for work
    for await (let [msg] of this.workRequestZ) {
      let work = JSON.stringify(this.decoder.decode(msg.buffer))
      this.logger.info(`Recv: ${work}`)
      this.authWS.send(work)
      if (this.isRunning === false) break
    }
  }
}
