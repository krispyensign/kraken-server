// eslint-disable-next-line @typescript-eslint/no-var-requires
require('source-map-support').install()
import * as conf from './config.json'
import { Config, ConnectionManager, LoggerFactoryService } from 'socket-comms-libs'
import yargs = require('yargs/yargs')
import { OrdersService } from './orders'
import * as kraken from 'kraken-helpers'
import { AddOrderStatus, CancelOrderStatus, SubscriptionStatus } from 'exchange-models/kraken'

const argv = yargs(process.argv.slice(2)).options({
  threshold: { type: 'number', default: conf.threshold },
  exchange: { type: 'string', default: conf.exchange },
  apiUrl: { type: 'string', default: conf.apiUrl },
  authWSEndpoint: { type: 'string', default: conf.authWSEndpoint },
  workRequestBind: { type: 'string', default: conf.workRequestBind },
  workResultBind: { type: 'string', default: conf.workResultBind },
}).argv

let loggerFactory = new LoggerFactoryService()
let connectionManager = new ConnectionManager((argv as unknown) as Config)

function getReqId(parsedEvent: AddOrderStatus | CancelOrderStatus | SubscriptionStatus): string {
  return parsedEvent.reqid?.toString() || '0'
}

let orderService = new OrdersService(loggerFactory.getLogger('OrdersService'), connectionManager, {
  isEvent: kraken.isStatusEvent,
  getReqId: getReqId,
})

orderService.startClient()
