"use strict";
var __asyncValues = (this && this.__asyncValues) || function (o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrdersService = void 0;
const kraken_helpers_1 = require("kraken-helpers");
class OrdersService {
    constructor(logger, connectionManager, exchangeAdapter) {
        this.isRunning = true;
        this.logger = logger;
        this.authWS = connectionManager.getAuthWS();
        this.encoder = connectionManager.getEncoder();
        this.decoder = connectionManager.getDecoder();
        this.workResponseZ = connectionManager.getWorkResponder();
        this.workRequestZ = connectionManager.getWorkConsumer();
        this.exchangeAdapter = exchangeAdapter;
    }
    async shutdown() {
        this.logger.info('Got shutdown');
        this.authWS.close();
        this.workRequestZ.close();
        this.workResponseZ.close();
        this.isRunning = false;
        this.logger.info('Shutdown complete');
    }
    async startClient() {
        var e_1, _a;
        // setup the message event
        this.authWS.on('message', async (eventData) => {
            // log the data
            this.logger.info(eventData);
            // parse it
            let parsedEvent = JSON.parse(eventData);
            // check if its a status event.  if so then get the reqid for the topic
            let reqid = '0';
            if (this.exchangeAdapter.isEvent(parsedEvent)) {
                this.logger.info(`Recv: ${eventData}`);
                reqid = this.exchangeAdapter.getReqId(parsedEvent);
            }
            // send the work response with the reqid as the topic
            await this.workResponseZ.send([reqid, this.encoder.encode(parsedEvent)]);
        });
        // wait for it to connect completely
        while (this.authWS.readyState === 0)
            await kraken_helpers_1.sleep(100);
        this.logger.info('Socket ready state: ' + this.authWS.readyState);
        this.logger.info('Waiting for shutdown');
        process.once('SIGINT', () => this.shutdown());
        process.once('SIGUSR2', () => this.shutdown());
        try {
            // start polling the zmq socket for work
            for (var _b = __asyncValues(this.workRequestZ), _c; _c = await _b.next(), !_c.done;) {
                let [msg] = _c.value;
                let work = JSON.stringify(this.decoder.decode(msg.buffer));
                this.logger.info(`Recv: ${work}`);
                this.authWS.send(work);
                if (this.isRunning === false)
                    break;
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) await _a.call(_b);
            }
            finally { if (e_1) throw e_1.error; }
        }
    }
}
exports.OrdersService = OrdersService;
//# sourceMappingURL=orders.js.map