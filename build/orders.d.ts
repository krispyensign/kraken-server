import { Logger } from 'winston';
import { ConnectionManager } from 'socket-comms-libs';
export interface ExchangeAdapter {
    getReqId(parsedEvent: unknown): string;
    isEvent(parsedEvent: unknown): boolean;
}
export declare class OrdersService {
    private readonly logger;
    private readonly authWS;
    private readonly encoder;
    private readonly decoder;
    private readonly workResponseZ;
    private readonly exchangeAdapter;
    private readonly workRequestZ;
    private isRunning;
    constructor(logger: Logger, connectionManager: ConnectionManager, exchangeAdapter: ExchangeAdapter);
    private shutdown;
    startClient(): Promise<Error | void>;
}
