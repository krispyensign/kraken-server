"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
// eslint-disable-next-line @typescript-eslint/no-var-requires
require('source-map-support').install();
const conf = __importStar(require("./config.json"));
const socket_comms_libs_1 = require("socket-comms-libs");
const yargs = require("yargs/yargs");
const orders_1 = require("./orders");
const kraken = __importStar(require("kraken-helpers"));
const argv = yargs(process.argv.slice(2)).options({
    threshold: { type: 'number', default: conf.threshold },
    exchange: { type: 'string', default: conf.exchange },
    apiUrl: { type: 'string', default: conf.apiUrl },
    authWSEndpoint: { type: 'string', default: conf.authWSEndpoint },
    workRequestBind: { type: 'string', default: conf.workRequestBind },
    workResultBind: { type: 'string', default: conf.workResultBind },
}).argv;
let loggerFactory = new socket_comms_libs_1.LoggerFactoryService();
let connectionManager = new socket_comms_libs_1.ConnectionManager(argv);
function getReqId(parsedEvent) {
    var _a;
    return ((_a = parsedEvent.reqid) === null || _a === void 0 ? void 0 : _a.toString()) || '0';
}
let orderService = new orders_1.OrdersService(loggerFactory.getLogger('OrdersService'), connectionManager, {
    isEvent: kraken.isStatusEvent,
    getReqId: getReqId,
});
orderService.startClient();
//# sourceMappingURL=index.js.map